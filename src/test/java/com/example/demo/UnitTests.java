package com.example.demo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UnitTests {
    @Test
    public void ezTest(){
        assertEquals(20, 20);
    }
    @Test
    public void checkSetAge(){
        Person p = new Person();
        p.setAge(20);
        assertEquals(p.getAge(), 20);
    }
}
